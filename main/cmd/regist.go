package cmd

/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0
*/

import (
	"encoding/json"
	"fmt"

	"chainmaker.org/chainmaker/tcip-fabric/v2/module/request"

	"github.com/spf13/pflag"

	"chainmaker.org/chainmaker/tcip-fabric/v2/module/conf"
	"chainmaker.org/chainmaker/tcip-fabric/v2/module/logger"
	"github.com/spf13/cobra"
)

// RegisterCMD 获取网关注册信息命令
//  @return *cobra.Command
func RegisterCMD() *cobra.Command {
	startCmd := &cobra.Command{
		Use:   "register",
		Short: "Out put register tcip-fabric info files",
		Long:  "Out put register tcip-fabric info files",
		RunE: func(cmd *cobra.Command, _ []string) error {
			initLocalConfig(cmd)
			Register()
			fmt.Println("tcip-fabric exit")
			return nil
		},
	}
	registerAttachFlags(startCmd, []string{flagNameOfConfigFilepath, flagNameOfObjectPathPath})
	return startCmd
}

// Register 获取网关注册信息命令
func Register() {
	cliLog = logger.GetLogger(logger.ModuleRegister)
	config, _ := json.Marshal(conf.Config)
	cliLog.Debug(string(config))

	_ = request.InitRequestManager()
	err := request.RequestV1.GatewayRegister(RegisterInfoPath)
	if err != nil {
		fmt.Println("register error: ", err)
	}
	fmt.Println("success")
	//resStr, _ := json.Marshal(res)
	//if res.Code == common.Code_GATEWAY_SUCCESS {
	//	fmt.Println("register success: ", string(resStr))
	//} else {
	//	fmt.Println("register error: ", string(resStr))
	//}
}

func registerFlagSet() *pflag.FlagSet {
	flags := &pflag.FlagSet{}
	flags.StringVarP(&conf.ConfigFilePath, flagNameOfConfigFilepath, flagNameShortHandOfConfigFilepath,
		conf.ConfigFilePath, "specify config file path, if not set, default use ./tcip_fabric.yml")
	flags.StringVarP(&RegisterInfoPath, flagNameOfObjectPathPath, flagNameShortHandOfObjectPathPath,
		RegisterInfoPath, "object path, default is ./register.json")
	return flags
}

func registerAttachFlags(cmd *cobra.Command, flagNames []string) {
	flags := registerFlagSet()
	cmdFlags := cmd.Flags()
	for _, flagName := range flagNames {
		if flag := flags.Lookup(flagName); flag != nil {
			cmdFlags.AddFlag(flag)
		}
	}
}
